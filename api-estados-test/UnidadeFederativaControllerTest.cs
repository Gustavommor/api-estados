using api_estados;
using api_estados.Controllers;
using Microsoft.Extensions.Logging;
using Xunit;
using Microsoft.EntityFrameworkCore;
using api_estados.Service;
using api_estados.Repository;

namespace api_estados_test
{
    public class UnidadeFederativaControllerTest
    {
        [Fact]
        public void PegaUnidadeFederativaTest()
        {
            ILogger _logger = CriarLogger();
            var context = CriaBancoContext("PegaUnidadeFederativaTest");
            IRepository<UnidadeFederativa> repository = new RepositoryUnidadeFederativa(context);
            IServiceUnidadeFederativa service = new ServiceUnidadeFederativa(repository);
            UnidadeFederativaController uc = new UnidadeFederativaController((ILogger<UnidadeFederativaController>)_logger, service);
            uc.CadastrarUnidade(MontaUnidade());
            Assert.NotEmpty(uc.UnidadesFederativa());
        }

        [Fact]
        public void PegaUnidadeCadastrada()
        {
            ILogger logger = CriarLogger();
            var context = CriaBancoContext("PegaUnidadeCadastrada");
            IRepository<UnidadeFederativa> repository = new RepositoryUnidadeFederativa(context);
            IServiceUnidadeFederativa service = new ServiceUnidadeFederativa(repository);
            UnidadeFederativaController uc = new UnidadeFederativaController(
                (ILogger<UnidadeFederativaController>)logger,service);
            UnidadeFederativa unid = MontaUnidade();
            uc.CadastrarUnidade(unid);
            Assert.Equal(unid.id, uc.UnidadesFederativa()[0].id);
        }

        private static UnidadeFederativa MontaUnidade()
        {
            return new UnidadeFederativa
            {
                id = 1,
                Nome = "Acre",
                UF = "AC",
            };
        }

        private static ILogger CriarLogger()
        {
            return new Logger<UnidadeFederativaController>(new LoggerFactory());
        }

        private BancoUnidadesContext CriaBancoContext(string name)
        {
            var options = new DbContextOptionsBuilder<BancoUnidadesContext>().
                UseInMemoryDatabase(databaseName: name).Options;

            return new BancoUnidadesContext(options);
        }
    }
}