using api_estados.Repository;
using api_estados.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace api_estados
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddTransient<IServiceUnidadeFederativa, ServiceUnidadeFederativa>();
            services.AddTransient<IRepository<UnidadeFederativa>, RepositoryUnidadeFederativa>();
            services.AddDbContext<BancoUnidadesContext>(options =>
                options.UseNpgsql(CarregaStringConexao()));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "api_estados", Version = "v1" });
            });
        }

        private string CarregaStringConexao()
        {
            var Id = Configuration["Id"];
            var Password = Configuration["Password"];
            var Host = Configuration["Host"];
            var Port = Configuration["Port"];
            var Database = Configuration["Database"];
            var Pooling = Configuration["Pooling"];

            return $"User ID={Id};Password={Password};Host={Host};Port={Port};Database={Database};Pooling={Pooling};";
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "api_estados v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
