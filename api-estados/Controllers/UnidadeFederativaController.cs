﻿using api_estados.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Immutable;

namespace api_estados.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UnidadeFederativaController : ControllerBase
    {
     
        private readonly ILogger<UnidadeFederativaController> _logger;
        private readonly IServiceUnidadeFederativa _service;

        public UnidadeFederativaController(ILogger<UnidadeFederativaController> logger, IServiceUnidadeFederativa service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        public IImmutableList<UnidadeFederativa> UnidadesFederativa()
        {
            return _service.GetUnidades();
        }

        [HttpPost]
        public int CadastrarUnidade(UnidadeFederativa unidade)
        {
            return _service.PostUnidade(unidade);
        }
    }
}
