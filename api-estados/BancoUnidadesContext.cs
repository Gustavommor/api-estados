﻿using Microsoft.EntityFrameworkCore;

namespace api_estados
{
    public class BancoUnidadesContext : DbContext
    {
        public DbSet<UnidadeFederativa> Unidades
        {
            get; set; 
        }
        public BancoUnidadesContext(DbContextOptions<BancoUnidadesContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UnidadeFederativa>().HasData(
                new UnidadeFederativa
                {
                    id = 1,
                    Nome = "Acre",
                    UF = "AC",
                }, 
                new UnidadeFederativa
                {
                    id = 2,
                    Nome = "Amapá",
                    UF = "AP"
                });
        }
    }
}
