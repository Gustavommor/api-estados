﻿using System.Collections.Immutable;

namespace api_estados.Repository
{
    public interface IRepository<T>
    {
        IImmutableList<T> Get();
        int Post(T element);
    }
}