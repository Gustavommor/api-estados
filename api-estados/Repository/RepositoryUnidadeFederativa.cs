﻿using System.Collections.Immutable;

namespace api_estados.Repository
{
    public class RepositoryUnidadeFederativa : IRepository<UnidadeFederativa>
    {
        private BancoUnidadesContext _context;
        public RepositoryUnidadeFederativa(BancoUnidadesContext context)
        {
            this._context = context;
        }

        public IImmutableList<UnidadeFederativa> Get()
        {
            return _context.Unidades.ToImmutableList();
        }

        public int Post(UnidadeFederativa unidadeFederativa)
        {
            _context.Unidades.Add(unidadeFederativa);
            _context.SaveChanges();
            return unidadeFederativa.id;
        }
    }
}
