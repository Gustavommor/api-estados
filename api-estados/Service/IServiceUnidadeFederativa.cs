﻿using System.Collections.Immutable;

namespace api_estados.Service
{
    public interface IServiceUnidadeFederativa
    {
        IImmutableList<UnidadeFederativa> GetUnidades();
        int PostUnidade(UnidadeFederativa unidade);
    }
}