﻿using api_estados.Repository;
using System.Collections.Immutable;

namespace api_estados.Service
{
    public class ServiceUnidadeFederativa : IServiceUnidadeFederativa
    {
        public IRepository<UnidadeFederativa> _repository;
        public ServiceUnidadeFederativa(IRepository<UnidadeFederativa> repository)
        {
            this._repository = repository;
        }

        public IImmutableList<UnidadeFederativa> GetUnidades()
        {
            return this._repository.Get();
        }

        public int PostUnidade(UnidadeFederativa unidade)
        {
            return _repository.Post(unidade);
        }
    }
}
