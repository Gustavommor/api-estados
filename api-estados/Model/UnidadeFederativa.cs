using System.ComponentModel.DataAnnotations.Schema;

namespace api_estados
{
    [Table("estados")]
    public class UnidadeFederativa
    {
        public int id { get; set; }

        public string UF { get; set; }

        public string Nome { get; set; }
    }
}
